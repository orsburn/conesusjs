const Conesus = require('./src/conesus');

const con = new Conesus('C:/Users/sorsburn.KLEINSTEEL/Development/AOP');

// -----------------------------------------------------------------------------
// con.log().then((output) => {
// 	console.log(output);
// });

// -----------------------------------------------------------------------------
// con.rawLog().then((output) => {
// 	console.log(output);
// });

// -----------------------------------------------------------------------------
con.logFiles().then((output) => {
	console.log(`         Hash:  ${output[232].hash}`);
	console.log(`       Status:  ${output[232].status}`);
	console.log(`        Score:  ${output[232].score}`);
	console.log(`         File:  ${output[232].file}`);
	console.log(`Previous file:  ${output[232].previousFile}`);
});

// -----------------------------------------------------------------------------
// con.commits().then((output) => {
// 	console.log(output);
// });

// -----------------------------------------------------------------------------
// con.commitForTag('1.6.22').then((output) => {
// 	console.log(output);
// });

// -----------------------------------------------------------------------------
// con.filesAtCommit('25b525').then((output) => {
// 	console.log(output);
// });

// -----------------------------------------------------------------------------
// con.filesSinceCommit('25b525').then((output) => {
// 	console.log(output);
// });