module.exports = class Conesus{
	constructor(repository = null){
		this.encoding = "utf-8";
		this.child_process = require("child_process");
		this.repository = repository; // arguments[0] || null;
		this.minHashLength = 4; // We can't be reasonably sure that we can match a hash unless we look at, at least, the first 4 characters.
	}

	/**
	 * Collects the files at a certain commit.
	 *
	 * @param string commitId
	 * @return array Returns an array of objects.
	 */
	filesAtCommit(commitId){
		const self = this;

		return this.logFiles({action: "logFiles"}).then((output) => {
			return output.filter((entry) => {
				if (
					(entry.hash === commitId) ||
					(entry.hash.substring(0, Math.max(commitId.length, self.minHashLength)) === commitId)
				){
					return true;
				}
			});
		});
	}

	/**
	 * Collects the files since a certain commit.
	 *
	 * @param string commitId
	 * @return array Returns an array of objects.
	 */
	filesSinceCommit(commitId){
		const self = this;

		if (commitId.length < this.minHashLength){
			return self.handleWarning("commitIdTooShort", commitId);
		}

		return this.commits()
			.then((commitIds) => {
				return self.getStopAtCommitId(commitId, commitIds);
			})
			.then((stopAtCommitId) => {
				const listing = [];

				return self.logFiles({action: "logFiles"}).then((output) => {
					for (let i = 0; i < output.length; i++){
						if ((output[i].hash === stopAtCommitId) || (stopAtCommitId === false)){
							return listing;
						}

						listing.push(output[i]);
					}

					return listing;
				});
			});
	}

	/**
	 * Fetches the history for the repository.
	 *
	 * @param boolean raw The function can also return the raw output from the git log command.  This will be returned as an array of entries, one commit per element.
	 * @return array Returns an array.
	 */
	log(raw = false){
		return this.exec(
			'git log --all --decorate --oneline',
			{action: "log", raw: !!raw}
		);
	}

	/**
	 * Fetches the history for the repository.
	 *
	 * @param boolean raw The function can also return the raw output from the git log command.  This will be returned as an array of entries, one commit per element.
	 * @return array Returns an array.
	 */
	rawLog(){
		return this.exec(
			'git log --all --decorate --oneline',
			{action: "rawLog"}
		);
	}

	logFiles(){
		return this.exec(
			'git log --all --name-status --oneline --pretty=format:"%H"',
			{action: "logFiles"}
		);
	}

	commits(raw = false){
		return this.exec(
			'git log --all --decorate --oneline --pretty=format:"%H"',
			{action: "commits", raw: !!raw}
		);
	}

	commitForTag(tag, raw = false){
		return this.exec(
			'git show-ref --tags --dereference',
			{action: "commitForTag", tag: tag, raw: !!raw}
		);
	}

	exec(command, options){
		options.cwd = options.cwd || this.repository;
		options.encoding = options.encoding || this.encoding;

		return new Promise((resolve, reject) => {
			if (!!options.cwd === false){
				return reject("The repository could not be found.\n");
			}

			// FIXME:  This may be called differently for Windows systems, so this will need some tweaking.  (Scott Orsburn | scott@orsburn. | 08/30/2018)
			const output = this.child_process.execSync(command, options).trim().split("\n");

			switch (options.action){
				case "log":
					resolve(this.handleLog(output, options));
					break;
				case "rawLog":
					resolve(output);
					break;
				case "logFiles":
					resolve(this.handleLogFiles(output, options));
					break;
				case "commits":
					resolve(output);
					break;
				case "commitForTag":
					resolve(this.handleCommitForTag(output, options));
					break;
			}
		});
	}

	handleLog(output, options){
		return output.map((element) => {
			const splitHere = element.indexOf(" ");
			return {hash: element.substring(0, splitHere), message: element.substring(splitHere + 1)}; // Plus 1 to skip the space we split on.
		});
	}

	handleLogFiles(output, options){
		const results = [];
		let hash;

		for (let i = 0; i < output.length; i++){
			const record = {};
			const current = output[i].split("\t");

			if (current.length >= 2){
				record.hash = hash;
				record.status = current[0];

				if ((current[0].charAt(0) === "R") || (current[0].charAt(0) === "C")){
					record.status = current[0].charAt(0);
					record.score = parseInt(current[0].substring(1));
				}

				if (current.length === 3){
					record.file = current[2];
					record.previousFile = current[1]; // If the file was renamed.
				}
				else {
					record.file = current[1];
				}

				results.push(record);
			}
			else if (current[0] !== ''){
				hash = current[0];
			}
		}

		return results;
	}

	handleCommitForTag(output, options){
		/*
		This method works the way it does because there no good way to handle the case
		where the tag being sought doesn't exist.  Any such checks would likely require
		multiple calls to git on the system.  It's probably faster to just parse some
		git output.

		Examples that have been tried:

			git tag -v [TAG]
			git rev-parse [TAG]
			git rev-list -n [TAG]
		*/
		let outputCommitId;

		output.forEach((element) => { // A forEach is used here so that we can break on the first instance.
			const broken = element.split(" ");
			const commitId = broken[0];
			const tagRef = broken[1];
			const tagCheck = tagRef.substring(tagRef.lastIndexOf("/") + 1);

			/*
			The entire list is scanned rather than trying to be clever about accommodating
			annotated tags.  Based on the listing from git log, it looks like the hash that
			should be output is the one with the annotation if one exists.
			*/
			if ((tagCheck === options.tag) || (tagCheck === `${options.tag}^{}`)){ // BUG  This will likely need to be updated to allow content between the curly braces.
				outputCommitId = commitId;
			}
		});

		return outputCommitId;
	}

	handleWarning(warning, details = null){
		switch (warning){
			case 'commitIdTooShort':
				return new Promise((resolve, reject) => {
					return reject(`The commit ID (${details}) is too short to reliably match against commit hashes.\n`);
				});
		}
	}

	/**
	 * Created specifically to support the "since commit" functions.
	 *
	 * It's a helper that takes a commit ID and a list of commit IDs, and returns the commit ID immediately after the desired one.
	 *
	 * @param {string} commitId
	 * @param {array} commitIds
	 *
	 * @returns It will return the commit ID immediately following the desired ID in an array of commit IDs.  Or it will return true if the desired ID is the last one in the list.  Otherwise, it will return false.
	 */
	getStopAtCommitId(commitId, commitIds){
		for (let i = 0; i < commitIds.length; i++){
			// We've found out commit ID.  And, there is an ID pass this one.
			if ((commitIds[i].substring(0, commitId.length) === commitId) && (commitIds[i + 1] || false)){
				return commitIds[i + 1];
			}
			// Otherwise, have we reached the end of the commit IDs list?
			else if (i === commitIds.length - 1){
				return true;
			}
		}

		return false;
	}
}