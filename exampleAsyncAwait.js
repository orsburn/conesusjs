const Conesus = require('./src/conesus');

(async function(){
	const con = new Conesus('C:/Users/sorsburn.KLEINSTEEL/Development/AOP');

	// -----------------------------------------------------------------------------
	// console.log(await con.log());

	// -----------------------------------------------------------------------------
	// console.log(await con.rawLog());

	// -----------------------------------------------------------------------------
	const output = await con.logFiles();

	console.log(`         Hash:  ${output[232].hash}`);
	console.log(`       Status:  ${output[232].status}`);
	console.log(`        Score:  ${output[232].score}`);
	console.log(`         File:  ${output[232].file}`);
	console.log(`Previous file:  ${output[232].previousFile}`);

	// -----------------------------------------------------------------------------
	// console.log(await con.commits());

	// -----------------------------------------------------------------------------
	// console.log(await con.commitForTag('1.6.22'));

	// -----------------------------------------------------------------------------
	// console.log(await con.filesAtCommit('25b525'));

	// -----------------------------------------------------------------------------
	// console.log(await con.filesSinceCommit('25b525'));
})();